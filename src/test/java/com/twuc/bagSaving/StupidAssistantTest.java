package com.twuc.bagSaving;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import static com.twuc.bagSaving.CabinetFactory.createCabinetWithPlentyOfCapacity;
import static org.junit.jupiter.api.Assertions.*;

class StupidAssistantTest {

    @Test
    void should_throw_when_saving_huge_bag() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class,
                        () -> stupidAssistant.save(new Bag(BagSize.HUGE)));
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#notHugeBagSize")
    void should_get_ticket_when_corresponding_locker_is_not_full(BagSize bagSize){
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);
        Ticket ticket = stupidAssistant.save(new Bag(bagSize));
        assertNotNull(ticket);
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#createCabinetWithOnlyOneSpecifyLocker")
    void should_throw_if_correspond_lockers_are_full(Cabinet fullCabinet, BagSize bagSize) {
        StupidAssistant stupidAssistant = new StupidAssistant(fullCabinet);
        Bag savedBag = new Bag(bagSize);
        InsufficientLockersException error = assertThrows(
                InsufficientLockersException.class,
                () -> stupidAssistant.save(savedBag));

        assertEquals("Insufficient empty lockers.", error.getMessage());
    }

    @Test
    void should_get_bag_when_give_right_ticket() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);

        Bag savedBag = new Bag(BagSize.BIG);
        Ticket ticket = stupidAssistant.save(savedBag);
        Bag getBag = stupidAssistant.getBag(ticket);

        assertSame(savedBag, getBag);
    }

    @Test
    void should_throw_if_no_ticket_is_provided() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);

        final IllegalArgumentException error = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.getBag(null));
        assertEquals("Please use your ticket.", error.getMessage());
    }

    @Test
    void should_throw_if_ticket_is_not_provided_by_cabinet() {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);

        Ticket ticket = new Ticket();

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#notHugeBagSize")
    void should_throw_if_ticket_is_used(BagSize bagSize) {
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);

        Bag bag = new Bag(bagSize);
        Ticket ticket = stupidAssistant.save(bag);
        cabinet.getBag(ticket);

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.getBag(ticket));
        assertEquals("Invalid ticket.", exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource("com.twuc.bagSaving.BagSavingArgument#notHugeBagSize")
    void should_throw_if_ticket_is_generated_by_another_cabinet(BagSize bagSize) {
        Cabinet anotherCabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant anotherStupidAssistant = new StupidAssistant(anotherCabinet);
        Cabinet cabinet = createCabinetWithPlentyOfCapacity();
        StupidAssistant stupidAssistant = new StupidAssistant(cabinet);

        Ticket generatedByAnotherCabinet = anotherStupidAssistant.save(new Bag(bagSize));

        final IllegalArgumentException exception = assertThrows(
                IllegalArgumentException.class,
                () -> stupidAssistant.getBag(generatedByAnotherCabinet));
        assertEquals("Invalid ticket.", exception.getMessage());
    }
}
