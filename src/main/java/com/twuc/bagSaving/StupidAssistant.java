package com.twuc.bagSaving;

public class StupidAssistant {
    private Cabinet cabinet;

    public StupidAssistant(Cabinet cabinet) {
        this.cabinet = cabinet;
    }

    public Ticket save(Bag bag) {
        LockerSize lockerSize;
        if(bag.getBagSize() == BagSize.SMALL || bag.getBagSize() == BagSize.MINI)
            lockerSize = LockerSize.SMALL;
        else if(bag.getBagSize() == BagSize.MEDIUM)
            lockerSize = LockerSize.MEDIUM;
        else
            lockerSize = LockerSize.BIG;

        return cabinet.save(bag, lockerSize);
    }

    public Bag getBag(Ticket ticket){
        return cabinet.getBag(ticket);
    }
}
